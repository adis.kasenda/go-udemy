package api

import (
	"database/sql"
	"udamy_golang/db"

	"github.com/gofiber/fiber/v2"
)

type Course struct {
	ID         string   `json:"id"`
	CourseCode string   `json:"course_code"`
	Title      string   `json:"title"`
	Subtitle   string   `json:"subtitle"`
	Creator    string   `json:"creator"`
	Language   string   `json:"language"`
	Price      int      `json:"price"`
	Topic      string 	`json:"topic"`
	CreatedAt  string   `json:"created_at"`
	UpdatedAt  string   `json:"updated_at"`
}

func GetCourseDetail(c *fiber.Ctx) error {
	id := c.Params("id")
	row := db.Db.QueryRow("SELECT * FROM course where id = $1", id)
	course := Course{}
	err := row.Scan(&course.ID, &course.CourseCode, &course.Title, &course.Subtitle, &course.Creator, &course.Language, &course.Price, &course.Topic, &course.CreatedAt, &course.UpdatedAt)

	switch {
	case err == sql.ErrNoRows:
		return c.JSON(&fiber.Map{
			"code":    404,
			"message": "Course Not Found!",
			"result":  "null",
		})
	case err != nil:
		return c.JSON(&fiber.Map{
			"code":    500,
			"message": err.Error(),
			"result":  "null",
		})
	default:
		 return c.JSON(&fiber.Map{
			"code":    200,
			"message": "Success",
			"result":  course,
		})
	}
}
