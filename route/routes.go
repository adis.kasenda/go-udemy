package route

import (
	"udamy_golang/api"

	"github.com/gofiber/fiber/v2"
)

func Route(app *fiber.App) {
	app.Get("/api/course/detail/:id", api.GetCourseDetail)
}
