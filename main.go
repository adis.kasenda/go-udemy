package main

import (
	"log"
	"udamy_golang/db"
	"udamy_golang/route"

	"github.com/gofiber/fiber/v2"
)

func main() {
	if err := db.Connect(); err != nil {
		log.Fatal(err)
	}

	app := fiber.New()

	route.Route(app)

	app.Listen(":3000")
}
